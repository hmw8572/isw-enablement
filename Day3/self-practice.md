# 프로젝트 완성하기 (design to deploy) (hands-on)

## Financial Position Log

![positionkeeping](images/position-keeping.png)

## Solution Designer

### Domain

1. RootEntity or Entity
    Financial Position Log - Root Entity 속성 설정

    | 이름                          | 속성                | 값                       |
    |----------------------|------------------------------|-----------------------------|
    |tansactionLogType             | Selection Element | [CREDIT, DEBIT]     |
    |transactionID       |  String   |  |
    |fee  | String | |
    |transactionStatus| Selection Element | [REQUESTED, COMPLETED, CANCELED]|
    |accountCurrency | String | |
    | openingDate | Date | |
    | externalService | String | |
    | externalServiceID |  String ||
    | timeStamp|  String |  |

2. Command
    CreateFinancialPositionLog - RootEntity 생성

### API
1. Path
    | 이름 | 값           | Operation
    | ---- | ------------ |-------------|
    | Path | /position-keeping | POST |

2. Request Body

    | 이름                          | 속성                |
    |----------------------|------------------------------|
    |transactionLogType| String |
    |transactionID| String |
    |transactionAmount| String|
    |accountNumber| String |
    |externalService | String |
    |externalServiceID | String |

3. Response
    | 이름                          | 속성                |
    |----------------------|------------------------------|
    |financialTransactionResult | String |

### Integration

1. Saving Account
    CreateDepositAndWithdrawals

      | 이름                          | 속성                | 값                       |
    |----------------------|------------------------------|-----------------------------|
    |depositType             | Selection Element | [CUSTOMER_DEPOSIT, INTERNAL_CREDIT, WITHDRAWAL_CANCELED]     |
    |transactionID       |  String   |  |
    |amount  | String | |
    |transactionDescription| String | |
    |withdrawalType | String | |
    | externalService | String | |
    | externalServiceID |  String ||
    | accountNumber|  String |  |

2. Internal Bank Account
   UpdateCaptureLog
    | 이름                          | 속성                |
    |----------------------|------------------------------|
    |externalService | String |
    |externalServiceID | String |
    |internalBankAccountLog | String |
    |internalBankAccountLogType | String |
    |timestamp | String |
    |accountNumber | String |
