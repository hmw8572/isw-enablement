# ISW Enablement 일정

## 1st Day

- Introduction

  | 시간                 | 일정                              |
  | -------------------- | --------------------------------- |
  | 13:30 ~ 14:20 (50분) | ISW 소개                          |
  | 14:30 ~ 15:20 (50분) | ISW enablement 개요               |
  | 15:30 ~ 16:50 (80분) | ISW 개발환경 setup하기 with GitPod |
  | 17:00 ~ 17:30        | Wrap up                           |

## 2nd Day

- ISW Solution Designer – DDD 모델 기반 서비스 설계, 개발하기 (Hands-on)

  | 시간                 | 일정                                           |
  | -------------------- | ---------------------------------------------- |
  | 10:00 ~ 11:20 (80분) | 실습내용 소개 및 Domain 디자인 실습 (따라하기) |
  | 11:30 ~ 13:00 (90분) | 점심시간                                       |
  | 13:00 ~ 13:50 (50분) | API 디자인 & 실습(따라하기)                    |
  | 14:00 ~ 14:50 (50분) | Integration 디자인 실습(따라하기)              |
  | 15:00 ~ 16:20 (80분) | Lowcode 기반 서비스 구축하기 (따라하기)        |
  | 16:20 ~ 16:50 (30분) | 빌드 / 테스트 / 배포 & OCP 파이프라인          |
  | 17:00 ~ 17:30        | Wrap up                                        |

## 3rd  Day

- 프로젝트 완성하기 (design to deploy) (hands-on)

  | 시간                      | 일정                                                             |
  | ------------------------- | ---------------------------------------------------------------- |
  | 10:00 ~ 11:30 (90분)      | Lowcode 기반 서비스 구축하기 (따라하기)    |
  | 11:30 ~ 13:00             | 점심시간       |
  | 13:00 ~ 13:30 (30분) | 빌드 / 테스트 / 배포 & OCP 파이프라인          |
  | 13:30 ~ 14:50 (80분)      |  Position Keeping Domain 설명  / Self Hands-on                 |
  | 15:00 ~ 15:30 (30분)      |  Instana를 통한 Observability 강화                                |
  | 15:30 ~ 16:00 (30분)       | Wrap up                                                          |

---

# ISW Enablement Contents

## 1st Day

### ISW 소개 : 홍규표

### ISW Enablemt : 김민희
  [ISW Enablement 개요](Day1/00-isw-enablment.md)

### 개발 환경 구성 : 류송희

## 2nd Day

### 실습 도메인 설명 : 장기숭

- Servicing Order Procedure  
  [참고](Day2/k5-design-practice.md)

- [ISW Layer 설명](Day2/intro.md)

### Domain Design : 장기숭

#### [용어 설명](Day2/domain.md)

#### Designer 설계

- Root Entity, Entity 설계
- Command 설계
- Service 설계
- Event, Agent 설계

### API Design : 장기숭

- path
- operations
- schema
- request
- response

### Integration Design : 장기숭

- Upload openapi spec file
- Service

### 개발하기 : 김민희

- [개발 가이드](Day2/k5-design-practice.md)

### CI/CD : 김민희

- tekton, openshift, debugging etc

## 3rd Day

### 실습 도메인 : 김민희

- Financial Position Log  
   [Position Keeping Service Domain](Day3/self-practice.md)

### Enhanced observability with Instana : 김민희

- Instana 설명 with ISW Projects
